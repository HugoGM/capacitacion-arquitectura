package mx.edu.uacm.capacitacion;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.edu.uacm.capacitacion.repository.DepartamentoJpaRepository;
import mx.edu.uacm.capacitacion.repository.EmpleadoJpaRepository;
import mx.edu.uacm.capacitacion.repository.ProyectoJpaRepository;

/**
 * Test empleado
 * @author Ana Laura Sosa, Hugo González
 *
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class EmpleadoTest {
	
	//Inyexión de dependencias del repositorio del empleado
	@Autowired
	private EmpleadoJpaRepository empleadoJpaRepository;

	//Inyexión de dependencias del repositorio del empleado
	@Autowired
	private DepartamentoJpaRepository departamentoJpaRepository;

	//Inyexión de dependencias del repositorio del empleado
	@Autowired
	private ProyectoJpaRepository proyectoJpaRepository;
	
	/**
	 * Preparando los registros antes de que se ejecuten los test
	 */
	@Before
	public void setUp() {
		
		Departamento departamento = crearDepartamento("Ventas", "sadasd");
		Proyecto proyecto = crearProyecto("Proyecto1", new Date(), new Date());
		List<Empleado> empleados = new ArrayList<>();
		List<Proyecto> proyectos = new ArrayList<>();
		
		
		
		Empleado empleado = creaEmpleado("Ana", "", "Sosa", "a.sosa0@gmail.com",
				new Date(), "5550651410", 29632.52);
				
				
		proyectos.add(proyecto);
		empleados.add(empleado);
		departamento.setEmpleados(empleados);
		proyecto.setEmpleados(empleados);
		empleado.setProyectos(proyectos);
		//empleadoJpaRepository.save(empleado);
		proyectoJpaRepository.save(proyecto);
		
		//departamentoJpaRepository.save(departamento);
		
		
		
	}
	
	/**
	 * Test para guardar un empleado
	 */
	@Test
	public void guardarEmpleado() {
		
		Departamento departamento = crearDepartamento("Ventas", "sadasd");
		Estacionamiento estacionamiento = new Estacionamiento();
		Proyecto proyecto = crearProyecto("Proyecto2", new Date(), new Date());
		List<Empleado> empleados = new ArrayList<>();
		List<Proyecto> proyectos = new ArrayList<>();
		
		Empleado empleado = creaEmpleado("Hugo", "", "González", "hugo.gonzalez.dv10@gmail.com",
				new Date(), "5529601421", 29632.52);
		
		empleado.setEstacionamiento(estacionamiento);
		
		proyectos.add(proyecto);
		empleados.add(empleado);
		departamento.setEmpleados(empleados);
		proyecto.setEmpleados(empleados);
		empleado.setProyectos(proyectos);
		
		
		assertThat(departamentoJpaRepository.save(departamento)).isNotNull();
		
	}
	
	/*
	 * Test para guardar borrar un empleado
	 */
	@Test
	public void borrarEmpleado() {
		
		Empleado empleado = empleadoJpaRepository.findById(1L).orElse(null);
		
		empleadoJpaRepository.delete(empleado);
	}
	
	/**
	 * Test para buscar un empleado por su id
	 */
	@Test
	public void buscarId() { 
		
		assertThat(departamentoJpaRepository.findById(1L)).isNotNull();
	}
	
	/**
	 * Test para traer todos los empleado
	 */
	@Test
	public void buscarTodo() {
		
		assertThat(empleadoJpaRepository.findAll().size()).isEqualTo(4);
	}
	
	/**
	 * Método auxiliar para crear un empleado
	 * @param nombre, nombre del empelado
	 * @param apellidoPaterno, primer apellido del empleado
	 * @param apellidoMaterno, segundo apellido del empleado
	 * @param email, correo electronico del empleado
	 * @param fechaContratacion, fecha de nacimiento del empleado
	 * @param telefono, teléfono del empleado
	 * @param salario, salario percibido por el empleado
	 * @return Empleado
	 */
	private Empleado creaEmpleado(String nombre, String apellidoPaterno, String apellidoMaterno, String email,
			Date fechaContratacion, String telefono, double salario) {
		Empleado empleado = new Empleado();
		empleado.setNombre(nombre);
		empleado.setApellidoPatermo(apellidoPaterno);
		empleado.setApellidoMaterno(apellidoMaterno);
		empleado.setEmail(email);
		empleado.setFechaContratacion(fechaContratacion);
		empleado.setTelefono(telefono);
		empleado.setSalario(salario);
		
		return empleado;
		
	}
	
	/**
	 * Método auxiliar para crear un departamento
	 * @param nombre, nombre del departamento
	 * @param locacion, localización gregrafica del departamento
	 * @return
	 */
	private Departamento crearDepartamento(String nombre, String locacion) {
		Departamento departamento = new Departamento();
		departamento.setNombre(nombre);
		departamento.setLocacion(locacion);
		
		return departamento;
	}
	
	/**
	 * 
	 * @param nombre, nombre del proyecto
	 * @param fechaInicio, fecha en el que inicio el proyecto
	 * @param fechaFin, fecha en la ue finzalizo el proyecto
	 * @return
	 */
	private Proyecto crearProyecto(String nombre, Date fechaInicio, Date fechaFin) {
		Proyecto proyecto = new Proyecto();
		proyecto.setNombre(nombre);
		proyecto.setFechaFin(fechaInicio);
		proyecto.setFechaInicio(fechaFin);
		return proyecto;
	}

}
