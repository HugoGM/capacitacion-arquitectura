package mx.edu.uacm.capacitacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjerciciosCapacitacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjerciciosCapacitacionApplication.class, args);
	}

}
