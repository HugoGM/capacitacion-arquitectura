package mx.edu.uacm.capacitacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import lombok.Data;

/**
 * Entidad Empleado
 * @author Ana Laura Sosa, Hugo González
 *
 */
@Entity
@Table(name = "empleados")
@Data
public class Empleado {

	@Id
	@Column(name = "id_empleado")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long idEmpleado;
		
	@Column(name = "nombre", nullable = false)
	public String nombre;
	
	
	@Column(name = "apellido_paterno", nullable = false)
	private String apellidoPatermo;
	
	@Column(name = "apellido_materno")
	public String apellidoMaterno;
	
	@Column(nullable = false)
	@Pattern(regexp = "^[a-zA-Z0-9.]+@[a-z.]+$", message = "Correo inválido")
	public String email;
	
	@Column(name = "fecha_contratacion", nullable = false)
	private Date fechaContratacion;
	
	@Column(nullable = false)
	@Pattern(regexp = "^([0-9]{8,10})$")
	private String telefono;
	
	@Column(nullable = false)
	private double salario;
	
	@ManyToOne
	private Departamento departamento;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Estacionamiento estacionamiento;
	
	@JoinTable(name = "empleados_proyectos",
			joinColumns = @JoinColumn(name = "id_empleado", nullable = false),
			inverseJoinColumns = @JoinColumn(name = "id_proyecto", nullable = false))
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Proyecto> proyectos = new ArrayList<>();
	
}
