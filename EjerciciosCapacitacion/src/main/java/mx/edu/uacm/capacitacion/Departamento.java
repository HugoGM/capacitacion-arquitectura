package mx.edu.uacm.capacitacion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

/**
 * Entidad Departamento
 * @author Ana Laura Sosa, Hugo González
 *
 */
@Entity
@Table(name = "departamentos")
@Data
public class Departamento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDepartamento;	
	
	@Column(nullable = false)
	private String nombre;
	
	@Column(nullable = false)
	private String locacion;
	
	@OneToMany(mappedBy = "departamento", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Empleado> empleados = new ArrayList<>();

}
