package mx.edu.uacm.capacitacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.edu.uacm.capacitacion.Empleado;

public interface EmpleadoJpaRepository extends JpaRepository<Empleado, Long> {

}
