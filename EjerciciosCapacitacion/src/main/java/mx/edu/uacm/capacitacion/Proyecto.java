package mx.edu.uacm.capacitacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

/**
 * Entidad Proyecto
 * @author Ana Laura Sosa, Hugo González
 *
 */
@Entity
@Table(name = "proyectos")
@Data
public class Proyecto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_proyecto")
	private Long idProyecto;
	
	private String nombre;
	
	@Column(name = "fecha_inicio")
	private Date fechaInicio;
	
	@Column(name = "fecha_fin")
	private Date fechaFin;
	
	@ManyToMany(mappedBy = "proyectos", cascade = CascadeType.ALL)
	private List<Empleado> empleados = new ArrayList<>();
	
	
}
