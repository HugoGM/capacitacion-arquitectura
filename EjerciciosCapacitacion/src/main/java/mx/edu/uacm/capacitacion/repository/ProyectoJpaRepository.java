package mx.edu.uacm.capacitacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.edu.uacm.capacitacion.Proyecto;

public interface ProyectoJpaRepository extends JpaRepository<Proyecto, Long> {

}
