package mx.edu.uacm.capacitacion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

/**
 * Entidad Departamento
 * @author Ana Laura Sosa, Hugo González
 *
 */
@Entity
@Data
public class Estacionamiento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estacionamiento")
	private Long idEstacionamiento;
	
	@OneToOne(mappedBy = "estacionamiento")
	private Empleado empleado;
	
}
