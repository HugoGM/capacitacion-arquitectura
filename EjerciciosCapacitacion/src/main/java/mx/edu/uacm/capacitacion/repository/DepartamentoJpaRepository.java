package mx.edu.uacm.capacitacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.edu.uacm.capacitacion.Departamento;

public interface DepartamentoJpaRepository extends JpaRepository<Departamento, Long> {

}
